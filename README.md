# Building REST API With Spring Boot, Spring Data MongoDB and Swagger

This is a tiny demo to demonstrate building RESTful services using Spring Boot and Spring Data MongoDB within a few lines.

Apart from traditional approach, the [Spring Data REST] `@RepositoryRestResource` offers a fully functional [HATEOAS] service
for free (without you having to implement either the repository methods or the REST service methods).

I also had a taste of integration testing with Spring Boot and `SpringClassRule` for Spring rule-based JUnit support
to allow for combination with third-party runners like JUnit's `Parameterized` runner.

## Domain
The domain classes may look somewhat weird. They come from one of my atomic cluster prediction utilities targeted at the
search for global minima of the given PES (Potential Energy Surface).

## Running the Demo
To start the app run: `gradle bootRun`

Swagger UI can be accessed at http://localhost:8080/swagger-ui.html.

[Spring Data REST]: http://projects.spring.io/spring-data-rest/
[HATEOAS]: https://spring.io/understanding/HATEOAS

