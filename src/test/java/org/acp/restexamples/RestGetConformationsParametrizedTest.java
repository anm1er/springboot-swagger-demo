package org.acp.restexamples;

import java.util.List;

import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.acp.restexamples.model.conformation.SelectedConformation;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@WebAppConfiguration
@IntegrationTest
public class RestGetConformationsParametrizedTest {

    public static final int SIZE_8 = 3;
    public static final int SIZE_12 = 4;
    public static final int SIZE_16 = 15;
    public static final double GO_8 = -19.32742;
    public static final double GO_12 = -36.40028;
    public static final double GO_16 = -53.84584;

    public static final double DELTA = 0.00001;
    public static final String GET_SELECTED_REQUEST = "http://localhost:8080/selected/";

    // SpringClassRule is a custom JUnit TestRule that supports class-level features of the Spring TestContext Framework.
    // In contrast to the SpringJUnit4ClassRunner, Spring's rule-based JUnit support has the advantage that it is independent
    // of any Runner and can therefore be combined with existing alternative runners like JUnit's Parameterized runner.
    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    private RestTemplate restTemplate = new TestRestTemplate();

    @Test
    @Parameters(method = "parametersForFindByNumberOfAtoms")
    public void findSelectedByNumberOfAtoms_NAtomsGiven_ShouldReturnKConformations(int atomsNumber, int fetchedNumber,
            double goPotential) {
        ResponseEntity<List<SelectedConformation>> responseEntity = restTemplate.exchange(GET_SELECTED_REQUEST + atomsNumber,
                HttpMethod.GET, null, new ParameterizedTypeReference<List<SelectedConformation>>() {
        });
        List<SelectedConformation> conformations = responseEntity.getBody();

        assertEquals(fetchedNumber, conformations.size());
        assertEquals(goPotential, conformations.get(0).getPotential(), DELTA);
    }

    // Provides the tuple for tests
    public Object[] parametersForFindByNumberOfAtoms() {
        return $(
                $(8, SIZE_8, GO_8),
                $(12, SIZE_12, GO_12),
                $(16, SIZE_16, GO_16)
        );
    }

}
