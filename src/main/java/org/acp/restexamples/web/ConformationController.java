package org.acp.restexamples.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.acp.restexamples.model.conformation.SelectedConformation;
import org.acp.restexamples.service.persistence.ConformationPersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/selected")
public class ConformationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConformationController.class);

    private final ConformationPersistenceService service;

    @Autowired
    ConformationController(ConformationPersistenceService service) {
        this.service = service;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    SelectedConformation delete(@PathVariable("id") String id) {
        return service.deleteSelected(id);
    }

    @RequestMapping(method = RequestMethod.GET) List<SelectedConformation> findAllPagedAndSorted(
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "properties", required = false) List<String> properties) {
        return service.findAllSelected(pageSize, properties);
    }

    @RequestMapping(value = "{n}", method = RequestMethod.GET) List<SelectedConformation> findByNumberOfAtoms(
            @PathVariable("n") int n) {
        return service.findSelectedByNumberOfAtoms(n);
    }

    @RequestMapping(value = "/go", method = RequestMethod.GET) List<SelectedConformation> findGOs() {
        return service.findAllSelectedGlobalMinima();
    }

}
