package org.acp.restexamples.model.conformation;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Conformation is a spatial arrangement of a molecule of a given configuration.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Conformation {

    @Id
    private String id;

    /**
     * Number of atoms in the conformation.
     */
    protected int n;

    /**
     * Pair potential value.
     */
    protected double potential;

    /**
     * Parent pair potential value.
     */
    protected double parent;

    /**
     * Rho parameter value.
     */
    protected int rho;

    /**
     * Relative nearest neighbour distance.
     */
    protected double nnd;

    public double getParentDelta() {
        return Math.abs(parent - potential);
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double getPotential() {
        return potential;
    }

    public void setPotential(double potential) {
        this.potential = potential;
    }

    public double getParent() {
        return parent;
    }

    public void setParent(double parent) {
        this.parent = parent;
    }

    public int getRho() {
        return rho;
    }

    public void setRho(int rho) {
        this.rho = rho;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getNnd() {
        return nnd;
    }

    public void setNnd(double nnd) {
        this.nnd = nnd;
    }

    @Override public String toString() {
        return new ToStringBuilder(this)
                .append("n", n)
                .append("potential", potential)
                .toString();
    }

}
