package org.acp.restexamples.model.conformation;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "conformations")
public class SelectedConformation extends Conformation {

    private double[] coordinates;

    /**
     * States whether this conformation refers to the putative global minimum.
     */
    private boolean isGO;

    public static Builder getBuilder() {
        return new Builder();
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public boolean isGO() {
        return isGO;
    }

    public void setIsGO(boolean isGO) {
        this.isGO = isGO;
    }

    public static class Builder {

        private SelectedConformation built;

        private Builder() {
            this.built = new SelectedConformation();
        }

        public Builder withCoordinates(double[] coordinates) {
            built.coordinates = coordinates;
            return this;
        }

        public Builder withNumberOfAtoms(int numberOfAtoms) {
            built.n = numberOfAtoms;
            return this;
        }

        public Builder withPotential(double potential) {
            built.potential = potential;
            return this;
        }

        public Builder withParentPotential(double potential) {
            built.parent = potential;
            return this;
        }

        public Builder withRelativeNNDistance(double distance) {
            built.nnd = distance;
            return this;
        }

        public Builder withRho(int rho) {
            built.rho = rho;
            return this;
        }

        public Builder isGlobalMinimum() {
            built.isGO = true;
            return this;
        }

        public SelectedConformation build() {
            return built;
        }
    }

}
