package org.acp.restexamples.service.persistence.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import org.acp.restexamples.model.conformation.SelectedConformation;
import org.acp.restexamples.repository.SelectedConformationRepository;
import org.acp.restexamples.service.persistence.ConformationPersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
final class ConformationPersistenceServiceImpl implements ConformationPersistenceService {

    public static final String DEFAULT_DIRECTION = "desc";

    private static final Logger LOGGER = LoggerFactory.getLogger(ConformationPersistenceServiceImpl.class);

    private final SelectedConformationRepository selectedRepository;

    @Value("${spring.data.rest.default-page-size}")
    private int defaultPageSize;

    @Autowired
    ConformationPersistenceServiceImpl(SelectedConformationRepository selectedRepository) {
        this.selectedRepository = selectedRepository;
    }

    @Override
    public SelectedConformation insertSelected(SelectedConformation conformation) {
        LOGGER.info("Inserting selected conformation");
        SelectedConformation inserted = selectedRepository.insert(conformation);
        LOGGER.info("Inserted selected conformation {}", conformation);
        return inserted;
    }

    @Override
    public int insertAllSelected(Collection<SelectedConformation> conformations) {
        LOGGER.info("Inserting {} selected conformations", conformations.size());
        List<SelectedConformation> inserted = selectedRepository.insert(conformations);
        LOGGER.info("Inserted {} selected conformations", inserted.size());
        return inserted.size();
    }

    @Override
    public SelectedConformation deleteSelected(String id) {
        LOGGER.info("Deleting a selected conformation with id: {}", id);
        SelectedConformation toBeDeleted = findSelectedById(id);
        selectedRepository.delete(toBeDeleted);
        LOGGER.info("Deleted selected conformation: {}", toBeDeleted);
        return toBeDeleted;
    }

    // MongoRepository implements PagingAndSortingRepository
    @Override
    public List<SelectedConformation> findAllSelected(Integer pageSize, List<String> properties) {
        if (pageSize == null) {
            pageSize = defaultPageSize;
        }
        if (properties == null) {
            return findAllSelectedPaged(pageSize);
        }
        return findAllSelectedPagedAndSorted(pageSize, properties);
    }

    @Override
    public List<SelectedConformation> findSelectedByNumberOfAtoms(int n) {
        LOGGER.info("Finding selected conformations with n: {}", n);
        List<SelectedConformation> conformations = selectedRepository.findByN(n);
        LOGGER.info("Found {} selected conformations", conformations.size());
        return conformations;
    }

    @Override
    public List<SelectedConformation> findAllSelectedGlobalMinima() {
        LOGGER.info("Finding selected GO conformations");
        List<SelectedConformation> conformations = selectedRepository.findByIsGOIsTrue();
        LOGGER.info("Found {} selected GO conformations", conformations.size());
        return conformations;
    }

    private List<SelectedConformation> findAllSelectedPaged(int pageSize) {
        LOGGER.info("Fetching {} selected conformations within a single page", pageSize);
        Page<SelectedConformation> selectedConformationPage = selectedRepository.findAll(new PageRequest(0, pageSize));
        LOGGER.info("All conformations that can be fetched: {}", selectedConformationPage.getTotalElements());
        return selectedConformationPage.getContent();
    }

    private List<SelectedConformation> findAllSelectedPagedAndSorted(int pageSize, List<String> properties) {
        LOGGER.info("Finding all selected conformations: {} within a single page and sorted by {}", pageSize, properties);
        Page<SelectedConformation> selectedConformationPage = selectedRepository.findAll(new PageRequest(0, pageSize,
                        Sort.Direction.fromString(DEFAULT_DIRECTION), properties.toArray(new String[0]))
        );
        LOGGER.info("Fetched {} selected conformations", selectedConformationPage.getTotalElements());
        return selectedConformationPage.getContent();
    }

    private SelectedConformation findSelectedById(String id) {
        SelectedConformation conformation = selectedRepository.findOne(id);
        if (conformation == null) {
            LOGGER.warn("Not found conformation with id {}", id);
            // or throw exception
        }
        return conformation;
    }

}
