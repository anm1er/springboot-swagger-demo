package org.acp.restexamples.service.persistence;

import java.util.Collection;
import java.util.List;

import org.acp.restexamples.model.conformation.SelectedConformation;

/**
 * Service to perform CRUD operations on {@code Conformation} objects.
 */
public interface ConformationPersistenceService {

    SelectedConformation insertSelected(SelectedConformation conformation);

    int insertAllSelected(Collection<SelectedConformation> conformations);

    SelectedConformation deleteSelected(String id);

    List<SelectedConformation> findAllSelected(Integer pageSize, List<String> properties);

    List<SelectedConformation> findSelectedByNumberOfAtoms(int n);

    List<SelectedConformation> findAllSelectedGlobalMinima();

}
