package org.acp.restexamples.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import org.acp.restexamples.model.conformation.SelectedConformation;

public interface SelectedConformationRepository extends MongoRepository<SelectedConformation, String> {

    List<SelectedConformation> findByN(int n);

    List<SelectedConformation> findByIsGOIsTrue();

}
