package org.acp.restexamples.repository.restdata;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import org.acp.restexamples.model.conformation.SelectedConformation;

/**
 * Presents an alternative way to create REST API with HATEOAS resources.
 * At runtime, Spring Data REST will create an implementation of this interface automatically.
 * Then it will use the @RepositoryRestResource annotation to direct Spring MVC to create RESTful endpoints at /conformatons.
 */
@Profile("hateoas")
@RepositoryRestResource(collectionResourceRel = "conformations", path = "conformations")
public interface RestDataSelectedConformationRepository extends MongoRepository<SelectedConformation, String> {

    List<SelectedConformation> findByN(@Param("n") int n);

}
